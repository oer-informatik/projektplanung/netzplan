## Einen Netzplan erstellen

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/netzplan</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/109426442684923187</span>

> **tl/dr;** _(ca. 5 min Lesezeit): Das Gantt-Diagramm ermöglicht die zeitliche Planung von Arbeitspaketen und Ressourcen. Wenn es im Projekt zu Verzögerungen kommt gibt uns ein anderes Diagramm Auskunft darüber, ob wir noch im Zeitplan bleiben können: das Netzplan-Diagramm._


Mit Hilfe eines Netzplans kann die zeitliche Abhängigkeit von Arbeitspaketen in einem Projekt bestimmt und dargestellt werden. Über Pufferzeiten und den kritischen Pfad können Kennwerte berechnet werden, mit deren Hilfe vorausgesagt werden kann, bei welchen Arbeitspaketen sich Verzögerungen direkt auf das Gesamtprojekt auswirken und wo nicht.

![Beispiel eines Netzplans](images/netzplan-komplett.jpg)

### Vorgangsliste anlegen

Um einen Netzplan zu erstellen, muss zunächst eine Vorgangsliste angelegt werden. Diese hat initial die folgenden Spalten (wird später noch ergänzt):

|Vorgangskennung|Vorgangsname|Dauer|Vorgänger Vorgangsnummer(n)|
|---|---|---|---|
|A|Anforderungen ermitteln|3||
|B|Anforderungen dokumentieren|3||
|C|Anforderungen validieren|3||
|D|Anforderungen verwalten|3||

Anstelle des "Vorgängers" kann auch über den Nachfolgevorgang die Reihenfolge definiert werden, wie etwa in dieser Vorgangsliste:

![Beispiel einer Vorgangsliste, bei der jeweils die Nachfolgetätigkeit benannt wird](images/vorgangsliste.png)

### Knoten anordnen

Für jeden Vorgang wird jetzt eine kleine Tabelle erstellt (4 Zeilen, 3 Spalten) und diese entsprechend ihrer Reihenfolge angeordnet und mit Pfeilen verbunden. Die Zellen Vorgangskennung (A-Z) oder Vorgangsnummer (Zahl), Name und Dauer können dabei schon ausgefüllt werden (in die mit `...` gekennzeichneten Zellen werden später berechnete Werte ergänzt):


|...||...|
|---|---|---|
|Kennung|Name des Vorgangs||
|Dauer|...|...|
|...||...|

Mit Hilfe einer Tabellenkalkulation kann dies beispielsweise so dargestellt werden:

![Beispiel eines Netzplanknotens (nur mit Nummer, Name und Dauer)](images/netzplan-detail.jpg)

Für die gesamte oben dargestellte Vorgangsliste sieht der so vorbereitete Netzplan dann folgendermaßen aus:

![Vorausgefüllter Netzplan mit allen Knoten, jedoch noch ohne berechnete Werte](images/netzplan-komplett-vorausgefuellt.jpg)

### Früheste Anfänge und Enden festlegen: Vorwärtsterminierung

Beginnend vom ersten Vorgang (der keine weiteren Vorgänger hat) werden jetzt die frühesten Anfangs- und Endpunkte festgelegt:

* Beim Ausgangsknoten ist der früheste Anfangspunkt null (FAZ= 0)
* Der früheste Endpunkt des ersten Knoten ist entspricht der Dauer (FEZ = FAZ + D)
* Bei allen Folgeknoten wird zunächst der Vorgängerknoten mit den _spätesten_ (höchsten) frühesten Endzeitpunkt (höchster FEZ) gesucht - dieser Wert stellt den FAZ dieses Knoten dar. ( FAZ(n) = max(FEZ(n-1)))
* Dann wird wieder jeweils der früheste Endzeitpunkt wie beim ersten Knoten bestimmt (FEZ = FAZ + D), bis die beiden Werte (FAZ und FEZ) bei allen Knoten ausgefüllt sind.

|FAZ(n) = max(FEZ(n-1)) ||FEZ = FAZ + D|
|---|---|---|
|Kennung|**Name des Vorgangs**||
|Dauer|...|...|
|...||...|

Für unsere Beispielvorgangsliste ergibt sich folgender Netzplan, an dem man u.a. das früheste Ende des Gesamtprojekts nach 26,5 Tagen (am letzten Knoten) ablesen kann:

![Netzplan mit Vorwärtsterminierung: FAZ und FEZ wurden eingetragen](images/netzplan-komplett-vorwaertsterminiert.jpg)

### Späteste Anfänge und Enden festlegen: Rückwärtsterminierung

 Jetzt werden ausgehend vom letzten Knoten (der keine Nachfolger mehr hat) die spätesten Anfangspunkte und spätesten Endpunkte berechnet:

 * Der späteste Endpunkt des letzten Knoten ist genau sein frühester Endpunkt (SEZ = FEZ).
 * Der jeweilige späteste Anfangspunkt wird berechnet aus spätestem Ende minus Dauer (SAZ = SEZ -D)
 * Für jeden weiteren Knoten wird der späteste Endzeitpunkt bestimmt aus dem _frühesten_ (kleinsten) spätesten Endzeitpunkt (SEZ) der Nachfolgeknoten. (SEZ(n) = min(SAZ(n+1)))  

|FAZ||FEZ|
|---|---|---|
|Kennung|**Name des Vorgangs**||
|Dauer|||
|SAZ = SEZ - D||SEZ(n) = min(SAZ(n+1))|

![Netzplan mit Rückwärtsterminierung: SAZ und SEZ wurden eingetragen](images/netzplan-komplett-rueckwaertsterminiert.jpg)

### Eintragung der Puffer

Bei mehreren Vorgängern mit unterschiedlichem FEZ oder mehreren Nachfolgern mit unterschiedlichem SAZ können Pufferzeiten entstehen, da auf das Ende von Vorgängen gewartet werden muss. Diese lassen sich folgendermaßen bestimmen:

* Der Gesamtpuffer gibt an, um wie viel ein Vorgang verschoben werden kann, ohne dass der Nachfolgevorgang den spätesten Anfangszeitpunkt überschreitet. Wird der Gesamtpuffer ausgenutzt, so verschiebt sich zwar der Nachfolgeprozess, nicht jedoch das Gesamtprojektende. Rechnerisch ergibt er sich aus der Zeit zwischen dem spätesten Anfangszeitpunkt und dem frühesten Anfangszeitpunkt (GP = SAZ - FAZ). (Rechnerisch ebenso aus SEZ-FEZ)
* Der Freie Puffer (FP) beschreibt denjenigen Puffer, bei dessen Ausnutzung der Nachfolgeprozess trotzdem zum geplanten Zeitpunkt starten kann. Solange nur der _Freie Puffer_ ausgenützt wird, ist kein weiterer Vorgang von der Verschiebung betroffen. Der FP entspricht der Zeit zwischen dem frühesten FAZ der Nachfolger und dem eigenen FEZ. (FP = min(FAZ(n+1))-FEZ(n))

|FAZ||FEZ|
|---|---|---|
|Kennung|**Name des Vorgangs**||
|Dauer|GP = SAZ - FAZ|FP(n) = min(FAZ(n+1))-FEZ(n)|
|SAZ||SEZ|

![komplett ausgefüllter Netzplan](images/netzplan-komplett.jpg)

Die Berechnungen der Werte können auch in der Vorgangsliste vorgenommen werden. Die komplette Vorgangsliste für das Ausgangsbeispiel könnte dann z.B. folgendermaßen aussehen:

![Beispiel einer Vorgangsliste, bei der jeweils die Nachfolgetätigkeit benannt wird](images/vorgangsliste-komplett.png)

Alle Rechenvorschriften zur Berechnung der Anfangs- und Endzeiten sowie der Puffer:

|FAZ(n) = max(FEZ(n-1)) ||FEZ = FAZ + D|
|---|---|---|
|Kennung|**Name des Vorgangs**||
|Dauer|GP = SAZ - FAZ|FP(n) = min(FAZ(n+1))-FEZ(n)|
|SAZ = SEZ - D||SEZ(n) = min(SAZ(n+1))|

### Welche Vorgänge bestimmen die Projektlaufzeit? Der kritische Pfad

Die Abfolge aller Vorgänge, die über keinen Puffer verfügen, nennt sich _kritischer Pfad_. Jede Verzögerung bei einem Vorgang im kritischen Pfad verzögert unmittelbar das Ende des Gesamtprojekts.

Im vorliegenden Pfad würde jede Verzögerung an einem Arbeitspaket auf dem Pfad ```A->B->C->D->E->H->K->M->N->O->P``` direkt zu einer Verzögerung des Projektendes führen, während die anderen Pfade noch über unterschiedlich große Puffer verfügen:

![komplett ausgefüllter Netzplan](images/netzplan-komplett-kritischerpfad.jpg)

### Einen eigenen Netzplan erstellen

Es gibt eine Reihe von spezialisierten Softwareprodukten, die Netzpläne aus Vorgangslisten erstellen und selbständig Kennziffern berechnen. Für kleinere Projekte ist ein einfacher Weg, Excel zu verwenden. Eine Vorlage zur Erstellung von Netzplänen mit Excel findet sich zum Beispiel auf der Seite von [Günter Schwindt](http://www.guenter-schwindt.de/organisation.htm) - die Vorlage ist allerdings im alten Excel-Format mit Macros erstellt und wir von einigen Virenscannern geblockt. Die Netzpläne auf dieser Seite wurden mit dieser Vorlage erstellt.


